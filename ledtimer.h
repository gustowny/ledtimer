//
// Libary for countdown timer for TM1637 based LED display.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef _LEDTIMER_H_
#define _LEDTIMER_H_

#include "Arduino.h"
#include "TM1637.h"
#include "buttonevent.h"
#include "statemachine.h"
#include "buzzer.h"
#include "localtimeobject.h"


// The LEDTimer class is a tick-driven count-down timer which displays the
// time om a LED display and is controlled by two buttons.
// 
// The following abbreviations are used in the descriptions below:
//  Ts = Time set, the inital (before count-down started) requested countdown time.
//  Tr = Time remaining, i.e. time until the alarm sounds.
//  Ti = Time since last user interaction, i.e. since last button was pressed.
//  Tp = Time delay until power save state is entered.
//  Tt = Period time between ticks.
//  B1 = Button 1, used for start, pause and reset.
//  B2 = Button 2, used for setting time.
//
class LEDTimer : public LocalTimeObject
{
   public:
   
      // Constructor, takes a TM1637 LED display as parameter.
      LEDTimer(TM1637& display, Buzzer& buzzer);
      
      // The LEDTimer can be in one of the following states:
      typedef enum {Idle,             // Displays Ts, Ts can be changed, Tr = Ts
                    Running,          // Displays and count down Tr
                    Paused,           // Displays Tr, no countdown, Tr can be changed
                    Alarm,            // Blinks Ts, sounds the alarm
                    PowerSaveIdle,    // The display is turned off
                    PowerSavePaused   // The display is turned off
                   } TimerStateType;
            
      // The LEDTimer functions are activated by calling the tick() member function
      // periodically, at least every 100 ms to make it work properly. The button events,
      // which are passed with the tick() function, controls the behaviour of the timer:
      //
      // Idle (inital state), Tr = Ts
      //  B1 clicked: -> Running state
      //  B1 hold   : Ts = 0 
      //  B2 clicked: Ts += 1 minute
      //  B2 hold   : Ts += 10 minutes per second
      //  Ti > Tp   : -> PowerSaveIdle state
      //
      // Running, Tr = Tr - Tt
      //  B1/B2 clicked: -> Paused state
      //  Tr = 0       : -> Alarm state
      //
      // Paused
      //  B1 clicked: -> Running state
      //  B1 hold   : -> Idle state, Ts = 0
      //  B2 clicked: Tr += 1 minute
      //  B2 hold   : Tr += 10 minutes per second
      //  Ti > Tp   : -> PowerSavePaused state
      //
      // Alarm, Tr = 0
      //  B1/B2 clicked: -> Idle state
      //
      // PowerSaveIdle
      //   B1/B2 clicked: -> Idle state
      //
      // PowerSavePaused
      //   B1/B2 clicked: -> Paused state
      //
      void tick(ButtonEvent button1, ButtonEvent button2);
      
      // Sets timer target time (Ts) and timer state to Idle.
      void setTime(int minutes, int seconds);
      
      // Sets time delay for power save state (Tp)
      void setPowerSaveDelay(int seconds);
      
      const int defaultPowerSaveDelaySeconds = 30;
      
   private:

      void run();
      void pause();
      void reset();
      void clear();
      void increaseSetTime();
      void increaseSetTimeFast();
      void increaseRemainingTime();
      void increaseRemainingTimeFast();
      void displayTime(int seconds);
      void alarm();
      void countDown();
            
      TM1637&        m_display;
      Buzzer&        m_buzzer;
      
      TimerStateType m_timerState;
      
      StateMachine   m_stateMachine;
      
      int            m_setTimeSeconds;
      int            m_lastDisplayedTimeSeconds;
      unsigned long  m_powerSaveDelaySeconds;
      unsigned long  m_timerTargetTime;
      unsigned long  m_timerRemainingTime;
      unsigned long  m_lastAlarmBlinkTime;
      unsigned long  m_lastFastSetTime;
      unsigned long  m_lastInteractionTime;
      bool           m_alarmBlinkOn;
      
      const unsigned long millisecondsPerSecond =  1000ul;
      const unsigned long alarmBlinkPeriodMs    =   500ul;
      const unsigned long fastSetPeriodMs       =   100ul;
                  
      const int           maxTimeSeconds        = 99 * 60 + 59;
      const unsigned long maxTimeMilliseconds   = (unsigned long)(maxTimeSeconds) * millisecondsPerSecond - 1;
      
};

#endif // _LEDTIMER_H_