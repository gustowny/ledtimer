//
// Libary for countdown timer for TM1637 based LED display.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "LEDTimer.h"


//--------------------------------------------------------------------------

LEDTimer::LEDTimer(TM1637& display, Buzzer& buzzer) :
   m_display(display),
   m_buzzer(buzzer),
   m_timerState(Idle),
   m_setTimeSeconds(0),
   m_lastDisplayedTimeSeconds(0),
   m_powerSaveDelaySeconds(0),
   m_timerTargetTime(0),
   m_timerRemainingTime(0),
   m_lastAlarmBlinkTime(0),
   m_lastFastSetTime(0),
   m_lastInteractionTime(0),
   m_alarmBlinkOn(false)
{
   m_powerSaveDelaySeconds = (unsigned long)(defaultPowerSaveDelaySeconds) * millisecondsPerSecond;
   
   m_buzzer.repeatOn();
}

//--------------------------------------------------------------------------

void LEDTimer::run()
{
   m_timerTargetTime     = localTime() + m_timerRemainingTime;
   m_lastInteractionTime = localTime();
   m_timerState          = Running;
}

//--------------------------------------------------------------------------

void LEDTimer::pause()
{
   m_timerRemainingTime	 = m_timerTargetTime - localTime();
   m_lastInteractionTime = localTime();
   m_timerState          = Paused;
}

//--------------------------------------------------------------------------

void LEDTimer::reset()
{
   m_timerRemainingTime = m_setTimeSeconds * millisecondsPerSecond;
   m_timerTargetTime    = 0;

   displayTime(m_setTimeSeconds);

   m_lastInteractionTime = localTime();
   m_timerState          = Idle;
}

//--------------------------------------------------------------------------

void LEDTimer::clear()
{
   m_setTimeSeconds     = 0;
   m_timerRemainingTime = 0;
   m_timerTargetTime    = 0;

   displayTime(m_setTimeSeconds);

   m_lastInteractionTime = localTime();
   m_timerState          = Idle;
}

//--------------------------------------------------------------------------

void LEDTimer::setTime(int minutes, int seconds)
{
   m_setTimeSeconds     = seconds + 60 * minutes;
   m_timerRemainingTime = m_setTimeSeconds * millisecondsPerSecond;
   m_timerTargetTime    = 0;
      
   displayTime(m_setTimeSeconds);

   m_lastInteractionTime = localTime();
   m_timerState          = Idle;
}

//--------------------------------------------------------------------------

void LEDTimer::tick(ButtonEvent button1, ButtonEvent button2)
{
   syncLocalTime(); // sync local time with system time
   
   switch (m_timerState)
   {
      case Idle:
      {
         if (button1 == Clicked)
         {
            reset();
            run(); 
         }
         else if (button2 == Clicked)
         {
            increaseSetTime();
         }
         else if (button2 == HoldDown)
         {
            increaseSetTimeFast();
         }
         else if (button1 == HoldDown)
         {
            clear(); 
         }
         else if ((localTime() - m_lastInteractionTime) > m_powerSaveDelaySeconds)
         {
            m_display.displayOff();
            m_timerState = PowerSaveIdle;
         }
      }
      break; 
      
      case Running:
      {	
         countDown(); 
         
         if (m_timerState == Running) // no alarm from countdown
         {
            if ((button1 == Clicked) || (button2 == Clicked))
            {
               pause(); 
            }
         }
      }
      break;
      
      case Paused:
      {
         if (button1 == Clicked)
         {
            run();
         }
         else if (button2 == Clicked)
         {
            increaseRemainingTime();
         }
         else if (button2 == HoldDown)
         {
            increaseRemainingTimeFast();
         }
         else if (button1 == HoldDown)
         {
            clear();
         }
         else if ((localTime() - m_lastInteractionTime) > m_powerSaveDelaySeconds)
         {
            m_display.displayOff();
            m_timerState = PowerSavePaused;
         }
      }
      break; 
      
      case Alarm:
      {
         alarm();

         if ((button1 == Clicked) || (button2 == Clicked))
         {
            m_buzzer.requestStop();
            reset();
         }
      }
      break; 
      
      case PowerSaveIdle:
      {   
         resetLocalTime(); // to avoid clock overflow
      
         if ((button1 == Clicked) || (button2 == Clicked))
         {
            m_display.displayOn();
            m_lastInteractionTime = localTime();
            m_timerState = Idle;
         }                  
      }
      break; 
      
      case PowerSavePaused:
      {
         resetLocalTime(); // to avoid clock overflow
         
         if ((button1 == Clicked) || (button2 == Clicked))
         {
            m_display.displayOn();
            m_lastInteractionTime = localTime();
            m_timerState = Paused;
         }                  
      }
      break;
      
   }

}

//--------------------------------------------------------------------------

void LEDTimer::displayTime(int seconds)
{
   const int min = seconds / 60;
   const int sec = seconds % 60;

   m_display.writeTime(min, sec);
   m_lastDisplayedTimeSeconds = seconds;
}

//--------------------------------------------------------------------------

void LEDTimer::countDown()
{
   int timeToDisplay = 0;
            
   if (m_timerTargetTime > localTime())
   {
      m_timerRemainingTime = m_timerTargetTime - localTime();
      timeToDisplay = (m_timerRemainingTime / millisecondsPerSecond) + 1;
   }
   else
   {
      m_timerRemainingTime = 0;
      timeToDisplay        = 0;
   }

   if (timeToDisplay != m_lastDisplayedTimeSeconds)
   {
      displayTime(timeToDisplay);
   }

   if (m_timerRemainingTime == 0)
   {
      m_lastAlarmBlinkTime = localTime();  // Delay blinking 1/2 period
      m_alarmBlinkOn       = true;        // 1st blink blank
      
      m_buzzer.requestPlay();
      
      m_timerState = Alarm;
   }

   m_lastInteractionTime = localTime();
}

//--------------------------------------------------------------------------

void LEDTimer::alarm()
{
   if (localTime() > (m_lastAlarmBlinkTime + (alarmBlinkPeriodMs / 2ul)))
   {
      m_alarmBlinkOn = !m_alarmBlinkOn;
      
      if (m_alarmBlinkOn)
      {
         const int min = m_setTimeSeconds / 60;
         const int sec = m_setTimeSeconds % 60;

         m_display.writeTime(min, sec);
      }
      else
      {
         m_display.clear();
      }
      
      m_lastAlarmBlinkTime = localTime();
   }

   m_lastInteractionTime = localTime();
}

//--------------------------------------------------------------------------

void LEDTimer::increaseSetTime()
{
   m_setTimeSeconds += 60;

   if (m_setTimeSeconds > maxTimeSeconds)
   {
      m_setTimeSeconds = maxTimeSeconds;
   }

   m_timerRemainingTime = (unsigned long)(m_setTimeSeconds) * millisecondsPerSecond;
         
   displayTime(m_setTimeSeconds);

   m_lastInteractionTime = localTime();
}

//--------------------------------------------------------------------------

void LEDTimer::increaseSetTimeFast()
{
   if (localTime() > (m_lastFastSetTime + fastSetPeriodMs))
   {
      increaseSetTime();
      m_lastFastSetTime = localTime();
   }
}

//--------------------------------------------------------------------------

void LEDTimer::increaseRemainingTime()
{
   m_timerRemainingTime += (60ul * millisecondsPerSecond);

   if (m_timerRemainingTime > maxTimeMilliseconds)
   {
      m_timerRemainingTime = maxTimeMilliseconds;
   }

   displayTime((m_timerRemainingTime / millisecondsPerSecond) + 1);

   m_lastInteractionTime = localTime();
}

//--------------------------------------------------------------------------

void LEDTimer::increaseRemainingTimeFast()
{
   if (localTime() > (m_lastFastSetTime + fastSetPeriodMs))
   {	
      increaseRemainingTime();
      m_lastFastSetTime = localTime();
   }
}

//--------------------------------------------------------------------------

void LEDTimer::setPowerSaveDelay(int seconds)
{
   m_powerSaveDelaySeconds = (unsigned long)(seconds) * millisecondsPerSecond;
}

